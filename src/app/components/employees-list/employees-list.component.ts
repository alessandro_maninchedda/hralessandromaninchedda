import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {
  // Initializes empty array of employees
  employees?: Employee[];
  // Initializes current employee's var
  currentEmployee?: Employee;
  // Sets required vars with values
  currentIndex = -1;
  name = '';

  constructor(private employeeService: EmployeeService) { }

  // Handle additional initialization task: Retrieve whole employee list
  ngOnInit(): void {
    this.retrieveEmployees();
  }

  // Imports all the employee list with getAll() method
  retrieveEmployees(): void {
    this.employeeService.getAll()
      .subscribe(
        data => {
          this.employees = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  //refreshes list in case of a deletion
  refreshList(): void {
    this.retrieveEmployees();
    this.currentEmployee = undefined;
    this.currentIndex = -1;
  }

  // Set current chosen employee as currentEmployee
  setActiveEmployee(employee: Employee, index: number): void {
    this.currentEmployee = employee;
    this.currentIndex = index;
  }

  // Reset current employee val
  setUnactiveEmployee(): void {
    this.currentEmployee = undefined;
    this.currentIndex = -1;
  }

  // Delete all employees from the list using method deleteAll() the refresh list.
  removeAllEmployees(): void {
    this.employeeService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  // Search functionality implemented with findByName() method instead of angular's pipe.
  searchName(): void {
    this.employeeService.findByName(this.name)
      .subscribe(
        data => {
          this.employees = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
