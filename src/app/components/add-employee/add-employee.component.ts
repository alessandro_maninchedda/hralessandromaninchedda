import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = {
    id_employee: "",
    name: "",
    picture: "",
    phone_number: "",
    email: "",
    hired_date: "",
    manager_id: "",
  };
  submitted = false;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  // Utilizes imported service' method to save new employee's information
  saveEmployee(): void {
    const data = {
      id_employee: this.employee.id_employee,
      name: this.employee.name,
      picture: this.employee.picture,
      phone_number: this.employee.phone_number,
      email: this.employee.email,
      hired_date: this.employee.hired_date,
      manager_id: this.employee.manager_id,
    };

    this.employeeService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  // Bool resetted and class re-initalized to receive a new employee's information.
  newEmployee(): void {
    this.submitted = false;
    this.employee = {
      id_employee: "",
      name: "",
      picture: "",
      phone_number: "",
      email: "",
      hired_date: "",
      manager_id: "",
    };
  }

}
