export class Employee {
  id?: any;
  id_employee?: string;
  name?: string;
  picture?: string;
  phone_number?: string;
  email?: string;
  hired_date?: string;
  manager_id?: string;
}
